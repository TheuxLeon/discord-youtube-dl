import youtube_dl
from subtitle import trim_subtitle

def download_info(youtube_link):
  title = '%(title)s.%(ext)s'
  with youtube_dl.YoutubeDL({
    'format': 'best',
    'subtitleslangs':['fr',]
    }) as ydl:
    info_dict = ydl.extract_info(youtube_link, download=False)
    return info_dict

def download(youtube_link, subtitle=False):
  info_dict = download_info(youtube_link)
  with youtube_dl.YoutubeDL({
    'outtmpl': "%s.%s" % (info_dict['title'], info_dict['ext']),
    'format': 'best',
    'writesubtitles': subtitle,
    'writeautomaticsub': subtitle,
    'subtitleslangs':['fr',]
    }) as ydl:
    ydl.download([youtube_link])
    return info_dict

def clean_subtitle(url_link):
  info_dict = download(url_link, subtitle=True)
  title = info_dict['title']
  with open("%s.fr.vtt" % title, "r", encoding="utf-8") as f:
    new_text = trim_subtitle(f.readlines())
  with open("%s_script.txt" % title, "w", encoding="utf-8") as f:
    f.write("\n".join(new_text))
    f.write("\n")
  return info_dict

if __name__ == '__main__':
  import cloud
  info = download("https://www.youtube.com/watch?v=YRQUyRQefMA&t=112s")
  link = cloud.upload_and_share("%s.%s" % (info['title'], info['ext']))
  print("Shared link ", link)
